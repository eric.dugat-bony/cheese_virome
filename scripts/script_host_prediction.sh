#!/bin/bash
# Shell to be used for job execution
#$ -S /bin/bash
# job name
#$ -N host_prediction
# q name
#$ -q short.q
# Export of environmental variables 
#$ -V
# Standard output
#$ -o log/host_prediction.out
# Error output
#$ -e log/host_prediction.err
# Run the command from the working directory
#$ -cwd
# Use 32 CPUs
#$ -pe thread 32

mkdir IPHOP

conda activate iphop-1.2.0
iphop predict --fa_file VIRAL_CONTIGS/viral_contigs.fasta --db_dir /db/outils/iphop-1.2.0/Sept_2021_pub/ --out_dir IPHOP/
conda deactivate
 

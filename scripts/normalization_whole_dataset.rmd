---
title: "normalization_whole_dataset"
author: "Eric DB"
date: "2023-10-31"
output: 
  html_document: 
    fig_caption: yes
    number_sections: yes
    theme: united
    toc: yes
    toc_float:
      collapsed: yes
      smooth_scroll: yes
  pdf_document:
    toc: yes
  word_document:
    toc: yes
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "~/work/metaviromes_Thomas/cheese_virome/")
```

# Description

This script enable to construct plyloseq objects from counts data obtained form cheese viromes (all samples from dynamic and persistence studies). Metadata associated to vOTUs include  iphop predictions, vibrant annotations, blat best hit against the dairy phages database and manual annotations. One phyloseq object containing raw counts and one containing transformed data (TPM: data rarefied, normalized by contig length and transformed to proportions) were produced and saved as R object in rds format to be used as entry in other scripts. Some basic analyses of the whole dataset are also included in this script. 

# Packages loading

```{r packages, message=FALSE, warning=FALSE}
library(tidyverse)
library(phyloseq)
library(DT)
library(ggVennDiagram)
library(cowplot)
sessionInfo()
```

# Data import

```{r data import, message=FALSE, warning=FALSE}
# import contig length
Contig_Length <- read.table("derep/contig_lengths_no_chimeras.tab") %>% dplyr::rename(Contig = V1, Length = V2)
# import count data for each sample
Data_2017_A <- read.table("MAPPING_BWA-MEM2/results.profile_2017_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2017_A = V2) 
Data_2017_B <- read.table("MAPPING_BWA-MEM2/results.profile_2017_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2017_B = V2)
Data_2017_C <- read.table("MAPPING_BWA-MEM2/results.profile_2017_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2017_C = V2)
Data_2019_A <- read.table("MAPPING_BWA-MEM2/results.profile_2019_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2019_A = V2) 
Data_2019_B <- read.table("MAPPING_BWA-MEM2/results.profile_2019_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2019_B = V2)
Data_2019_C <- read.table("MAPPING_BWA-MEM2/results.profile_2019_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2019_C = V2)
Data_2022_A <- read.table("MAPPING_BWA-MEM2/results.profile_2022_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2022_A = V2) 
Data_2022_B <- read.table("MAPPING_BWA-MEM2/results.profile_2022_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2022_B = V2)
Data_2022_C <- read.table("MAPPING_BWA-MEM2/results.profile_2022_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, y2022_C = V2)
Data_W1_A <- read.table("MAPPING_BWA-MEM2/results.profile_W1_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W1_A = V2) 
Data_W1_B <- read.table("MAPPING_BWA-MEM2/results.profile_W1_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W1_B = V2)
Data_W1_C <- read.table("MAPPING_BWA-MEM2/results.profile_W1_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W1_C = V2)
Data_W2_A <- read.table("MAPPING_BWA-MEM2/results.profile_W2_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W2_A = V2) 
Data_W2_B <- read.table("MAPPING_BWA-MEM2/results.profile_W2_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W2_B = V2)
Data_W2_C <- read.table("MAPPING_BWA-MEM2/results.profile_W2_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W2_C = V2)
Data_W3_A <- read.table("MAPPING_BWA-MEM2/results.profile_W3_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W3_A = V2) 
Data_W3_B <- read.table("MAPPING_BWA-MEM2/results.profile_W3_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W3_B = V2)
Data_W3_C <- read.table("MAPPING_BWA-MEM2/results.profile_W3_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W3_C = V2)
Data_W4_A <- read.table("MAPPING_BWA-MEM2/results.profile_W4_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W4_A = V2) 
Data_W4_B <- read.table("MAPPING_BWA-MEM2/results.profile_W4_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W4_B = V2)
Data_W4_C <- read.table("MAPPING_BWA-MEM2/results.profile_W4_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W4_C = V2)
Data_W5_A <- read.table("MAPPING_BWA-MEM2/results.profile_W5_A_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W5_A = V2) 
Data_W5_B <- read.table("MAPPING_BWA-MEM2/results.profile_W5_B_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W5_B = V2)
Data_W5_C <- read.table("MAPPING_BWA-MEM2/results.profile_W5_C_proportional.txt", skip =9) %>% dplyr::rename(Contig = V1, W5_C = V2)
# Join data into one abundance table
Data <- full_join(Data_2017_A, Data_2017_B, by = "Contig") %>%
  full_join(Data_2017_C, by = "Contig") %>%
  full_join(Data_2019_A, by = "Contig") %>%
  full_join(Data_2019_B, by = "Contig") %>%
  full_join(Data_2019_C, by = "Contig") %>%
  full_join(Data_2022_A, by = "Contig") %>%
  full_join(Data_2022_B, by = "Contig") %>%
  full_join(Data_2022_C, by = "Contig") %>%
  full_join(Data_W1_A, by = "Contig") %>%
  full_join(Data_W1_B, by = "Contig") %>%
  full_join(Data_W1_C, by = "Contig") %>%
  full_join(Data_W2_A, by = "Contig") %>%
  full_join(Data_W2_B, by = "Contig") %>%
  full_join(Data_W2_C, by = "Contig") %>%
  full_join(Data_W3_A, by = "Contig") %>%
  full_join(Data_W3_B, by = "Contig") %>%
  full_join(Data_W3_C, by = "Contig") %>%
  full_join(Data_W4_A, by = "Contig") %>%
  full_join(Data_W4_B, by = "Contig") %>%
  full_join(Data_W4_C, by = "Contig") %>%
  full_join(Data_W5_A, by = "Contig") %>%
  full_join(Data_W5_B, by = "Contig") %>%
  full_join(Data_W5_C, by = "Contig")
```

# Transform to phyloseq object

```{r table to phyloseq, message=FALSE, warning=FALSE}
# abundance table
abundance <- column_to_rownames(Data,'Contig')
colSums(abundance)
# sample metadata
metadata <- read.csv("METADATA/Metadata.txt", sep = "\t", row.names = 1) 
# viral contigs info
## iphop annotations
iphop <- read.table("IPHOP/Host_prediction_to_genus_m90.csv", sep =",", header = T) %>%
  dplyr::rename(Contig = Virus, Host_genus_iphop = Host.genus, Confidence_score_iphop = Confidence.score) %>%
  select(Contig, Host_genus_iphop, Confidence_score_iphop) %>%
  group_by(Contig) %>%
  filter(!duplicated(Contig)) %>%
  separate_wider_delim(cols = Host_genus_iphop, delim = ";", names = c("Host_domain_iphop", "Host_phylum_iphop", "Host_class_iphop", "Host_order_iphop", "Host_family_iphop", "Host_genus_iphop")) %>%
  mutate(Host_domain_iphop = str_replace_all(Host_domain_iphop, "d__", "")) %>%
  mutate(Host_phylum_iphop = str_replace_all(Host_phylum_iphop, "p__", "")) %>%
  mutate(Host_phylum_iphop = str_replace_all(Host_phylum_iphop, "_A", "")) %>%
  mutate(Host_class_iphop = str_replace_all(Host_class_iphop, "c__", "")) %>%
  mutate(Host_order_iphop = str_replace_all(Host_order_iphop, "o__", "")) %>%
  mutate(Host_order_iphop = str_replace_all(Host_order_iphop, "_H", "")) %>%
  mutate(Host_family_iphop = str_replace_all(Host_family_iphop, "f__", "")) %>%
  mutate(Host_family_iphop = str_replace_all(Host_family_iphop, "_G", "")) %>%
  mutate(Host_genus_iphop = str_replace_all(Host_genus_iphop, "g__", "")) %>%
  mutate(Host_genus_iphop = str_replace_all(Host_genus_iphop, "_A", "")) %>%
  mutate(Host_genus_iphop = str_replace_all(Host_genus_iphop, "_B", "")) %>%
  mutate(Host_genus_iphop = str_replace_all(Host_genus_iphop, "_E", ""))
## CheckV annotations
checkV <- read.table("CHECKV/quality_summary.tsv", sep="\t", header = T) %>%
  dplyr::rename(Contig = contig_id, checkv_completeness = completeness) %>%
  select(Contig, checkv_quality, checkv_completeness)
## vibrant annotations
vibrant <- read.table("VIBRANT/VIBRANT_contigs_derep/VIBRANT_results_contigs_derep/VIBRANT_genome_quality_contigs_derep.tsv", sep="\t", header = T) %>%
  dplyr::rename(Contig = scaffold, Lifestyle = type, Quality_vibrant = Quality) %>%
  filter(Quality_vibrant!='complete circular')
vibrant$Lifestyle[vibrant$Lifestyle=='lytic'] <- 'Virulent'
vibrant$Lifestyle[vibrant$Lifestyle=='lysogenic'] <- 'Temperate'
## manual annotations
manual_annot <- read.table("METADATA/Contigs_characteristics3.txt", sep ="\t", header = T) %>%
  dplyr::rename(Contig = X, Type_manual = Type, Closest_relatives_manual = Closest_relatives, Host_genus_manual = Host_genus) %>%
  select(Contig, Type_manual, Closest_relatives_manual, Host_genus_manual)
## blat annotations
dairy_phage_db <- read.table("Dairy_phages_database/Dairy_phages_genome_metadata.txt", sep="\t", header = TRUE)
blat_annot <- read.table("BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_30.tab", sep="\t") %>%
  dplyr::rename(Contig = V1, Accession_number = V2, Blat_alignment_lenght = V3, Blat_query_length = V4, Blat_score = V5) %>%
  left_join(dairy_phage_db, by = "Accession_number") %>%
  dplyr::rename(BH_Accession_number = Accession_number, BH_lifestyle = Lifestyle, Host_genus_blat = Host_genus)
## remove duplicated_lines in blat_annot
blat_annot2 <- blat_annot[-c(60,86,104),]
## correspondence genus phylum
genus_phylum <- read.table("METADATA/Genus_phylum.txt", sep="\t", header = T)
## join all tables
contigs_info <- read.table("VIRAL_CONTIGS/List_contigs_union.txt") %>%
  dplyr::rename(Contig = V1) %>%
  left_join(y = Contig_Length, by = "Contig") %>%
  left_join(y = checkV, by = "Contig") %>%
  left_join(y = vibrant, by = "Contig") %>%
  left_join(y = iphop, by = "Contig") %>%
  left_join(y = manual_annot, by = "Contig") %>%
  left_join(y = blat_annot2, by = "Contig") %>% 
  mutate(Host_genus = coalesce(Host_genus_blat, Host_genus_iphop)) %>%
  left_join(y = genus_phylum, by = "Host_genus") %>% 
  column_to_rownames(var = "Contig") 
contigs_info$Group <- ifelse(is.na(contigs_info$Group), "No hit", contigs_info$Group)
contigs_info$Host_genus <- ifelse(is.na(contigs_info$Host_genus), "No prediction", contigs_info$Host_genus)
contigs_info$Lifestyle <- ifelse(is.na(contigs_info$Lifestyle), "No prediction", contigs_info$Lifestyle)
contigs_info <- as.matrix(contigs_info) 
write.table(as.data.frame(contigs_info), file = "METADATA/contigs_info.txt", sep = "\t", row.names = T, quote = F, col.names = NA)
# Check consistency
all(colnames(abundance) %in% rownames(metadata)) ## sample names between metadata and otu table
all(rownames(abundance) %in% rownames(contigs_info)) ## taxa names between taxonomic and otu tables
# phyloseq object
obj <- phyloseq(otu_table(abundance, taxa_are_rows = TRUE), sample_data(metadata) , tax_table(contigs_info))
# remove PhiX
badTaxa <- "2017-0_NODE45"
allTaxa <- taxa_names(obj)
allTaxa <- allTaxa[!(allTaxa %in% badTaxa)]
obj <- prune_taxa(allTaxa, obj)
obj
saveRDS(obj, file = "PHYLOSEQ_ANALYSIS/obj_raw_whole.rds")
```

# Density histogram

```{r density histogram, message=FALSE, warning=FALSE}
# extract contigs data from phyloseq object
c_data <- tax_table(obj) %>% as.data.frame() %>% mutate_at('Length', as.numeric)
# draw density histogram
p_dens <- ggplot(c_data, aes(x=(Length/1000))) +
  geom_histogram(colour="black", fill="#a6bddb", binwidth = 2) + 
  theme_bw() +
  ylab("Number of vOTUs") + 
  xlab("Size (Kb)") +
  scale_x_continuous(breaks=seq(0, 150, 10)) +
  theme(axis.text.x = element_text(size = 8),
        axis.text.y = element_text(size = 8))
p_dens 
```

Determine the number oh contigs >10 Kb:
```{r}
n_sup_10Kb <- sum(c_data$Length > 10000)
n_sup_10Kb
```

# Plot checkV quality 

```{r checkV quality, message=FALSE, warning=FALSE}
p_checkV <- ggplot(c_data, aes(x = "", fill = checkv_quality)) +
  geom_bar(width = 1, stat = "count") +
  coord_polar(theta = "y") +
  geom_text(stat = "count", aes(label = sprintf("%.1f%%", (..count..)/sum(..count..)*100)),
            position = position_stack(vjust = 0.5)) +
  scale_fill_discrete(name = "Qualité") +
  theme_minimal() + 
  theme(axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_blank())
p_checkV
#Table
table_checkv_quality <- table(c_data$checkv_quality)
print(table_checkv_quality)
```

# Host prediction statistics

```{r host prediction, message=TRUE, warning=TRUE}
# table
table_host_pred <- table(c_data$Host_genus)
print(table_host_pred)
# barplot
Host_phylum_colors=c("Bacillota"="#78c679",
                    "Actinomycetota"="#dd1c77",
                    "Pseudomonadota" = "#6a51a3")
c_data_ord <- mutate(c_data, Host_genus = factor(Host_genus, levels=c("Lactococcus", "Leuconostoc", "Pseudoalteromonas", "Psychrobacter", "Brevibacterium", "Glutamicibacter", "Halomonas", "Streptococcus", "Vibrio", "Acinetobacter", "Cobetia", "Paenibacillus", "Agathobacter", "Bacillus", "Enterococcus", "Staphylococcus", "No prediction")))
p_host <- ggplot(c_data_ord, aes(x = Host_genus, fill = Host_phylum)) +
  geom_bar(color = "black", position = "identity") +
  scale_fill_manual(values = Host_phylum_colors) +
  labs(x = "Host genus",
       y = "Number of vOTUs",
       fill = "Host phylum") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle = 45, hjust = 1, size = 8),
        legend.position = c(0.7, 0.8),
        axis.text.y = element_text(size = 8),
        legend.title = element_text(size=10),
        legend.text = element_text(size=8)) 
p_host
```

# Rarefaction

```{r rarefaction, message=FALSE, warning=FALSE}
min(sample_sums(obj))
obj_rare <- rarefy_even_depth(obj, sample.size = min(sample_sums(obj)), rngseed = 1234)
sample_sums(obj_rare)
obj_rare
```

# TPM transformation

```{r TPM, message=FALSE, warning=FALSE}
# Extract rarefied abundance table
rare_abundance_table <- as.data.frame(otu_table(obj_rare)) %>% rownames_to_column(var = "Contig")
# Add contig length and set the vector of contig length
rare_abundance_table2 <- left_join(rare_abundance_table, Contig_Length, by = "Contig")
vect_length <- rare_abundance_table2$Length 
# Transform counts to TPM
## First divide the count matrix by the vector of contig length
obj_TPM_whole <- obj_rare
otu_table(obj_TPM_whole) <- otu_table(obj_TPM_whole) / vect_length 
## Second transform the count matrix into proportions 
count_to_prop <- function(x) {return( x / sum(x) )}
obj_TPM_whole <- transform_sample_counts(obj_TPM_whole, count_to_prop)
sample_sums(obj_TPM_whole)
head(otu_table(obj_TPM_whole))
```

# Export phyloseq object

```{r export, message=FALSE, warning=FALSE}
saveRDS(obj_TPM_whole, file = "PHYLOSEQ_ANALYSIS/obj_TPM_whole.rds")
```

# Produce a sorted abundance table based on raw counts

```{r sorted abundance table raw, message=FALSE, warning=FALSE}
# Transform raw counts to proportions
obj_rare_prop <- transform_sample_counts(obj_rare, count_to_prop)
sample_sums(obj_rare_prop)
head(otu_table(obj_rare_prop))
# Merge the data table with vOTUs annotations
tax2 <- as.data.frame(tax_table(obj_rare_prop)) %>% 
  rownames_to_column("Contigs")
contig_table2 <- as.data.frame(otu_table(obj_rare_prop))
contig_table2 <- mutate(contig_table2, Average = rowMeans(contig_table2)) %>%
  arrange(desc(Average)) %>%
  rownames_to_column("Contigs") %>%
  right_join(tax2, by = "Contigs")
# Produce interactive tables with DT
datatable(contig_table2, filter = 'top',extensions = 'Buttons', options = list(dom = 'Bfrtip',buttons = c('copy', 'csv', 'excel', 'pdf', 'print')))
#data_summary <- group_by(contig_table2, Group) %>% summarize(sum = sum(Average)) %>% arrange(desc(sum))
#datatable(data_summary, filter = 'top',extensions = 'Buttons', options = list(dom = 'Bfrtip',buttons = c('copy', 'csv', 'excel', 'pdf', 'print')))
```

# Determine the number of abundant contigs (>0.00005) in the whole dataset

```{r abundant}
#Abundance > 0.00005 filter
Ab = 5e-5
x = taxa_sums(obj_TPM_whole)/24
keepTaxa = x > Ab
prunedSet_whole = prune_taxa(keepTaxa, obj_TPM_whole)
prunedSet_whole
```

# Determine the proportion of common vOTUs between the samples included in the dynamics study and the samples for the persitence study

```{r common, message=FALSE, warning=FALSE}
# dyn data
obj_raw_dyn <- subset_samples(obj, Wash_type %in% c("S","A"))
obj_raw_dyn
Ab = 0
x = taxa_sums(obj_raw_dyn)
keepTaxa = x > Ab
prunedSet_dyn <- prune_taxa(keepTaxa, obj_raw_dyn)
prunedSet_dyn
# pers data
obj_raw_pers <- subset_samples(obj, Year %in% c("y2017","y2019","y2022"))
obj_raw_pers
y = taxa_sums(obj_raw_pers)
keepTaxa_y = y > Ab
prunedSet_pers <- prune_taxa(keepTaxa_y, obj_raw_pers)
prunedSet_pers
# creates vector of contigs present in one or the other dataset
Contigs_dyn <- phyloseq::taxa_names(prunedSet_dyn)
Contigs_pers <- phyloseq::taxa_names(prunedSet_pers)
#Make a list
list_contigs_c <- list(dyn = Contigs_dyn, pers = Contigs_pers)
#plot the venn diagram
p1 <- ggVennDiagram(list_contigs_c, 
                    category.names = c("Dynamics", "Persistence"), 
                    set_size = 2.2,
                    label_size = 2.5,
                    label_percent_digit = 1)
p1
```

# generate final figure

Figure 2 of the article :

```{r figure 2, message=FALSE, warning=FALSE, fig.height= 4, fig.width = 9}
# Figure beta-div + alpha-div + pheatmap
p_fig2 <- plot_grid(p_dens, p_host, labels=c("A", "B"), ncol = 2, nrow = 1)
p_fig2
ggsave("PHYLOSEQ_ANALYSIS/Figure_density_host.tiff", units="in", width=9, height=4, dpi=300, compression = 'lzw')
```

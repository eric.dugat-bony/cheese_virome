#!/bin/bash
# Shell Ã  utiliser pour l'exÃ©cution du job
#$ -S /bin/bash
# Nom du job
#$ -N blat_dairy_phages_best_hit
# Nom de la queue
#$ -q short.q
# Export de toutes les variables d'environnement
#$ -V
# Sortie standard
#$ -o log/blat_dairy_phages_best_hit.out
# Sortie dâ€™erreur
#$ -e log/blat_dairy_phages_best_hit.err
# Lance la commande depuis le rÃ©pertoire oÃ¹ est lancÃ© le script
#$ -cwd
# Utiliser 16 CPUs
#$ -pe thread 16

# Step 1: for contig headers, only keep what's before the first space (spaces are used as separators for the rest of the script)
mkdir BLAT_DAIRY_PHAGES_DB
sed 's/^\(>[^ ]*\) .*/\1/g' VIRAL_CONTIGS/viral_contigs.fasta > BLAT_DAIRY_PHAGES_DB/viral_contigs_clean.fasta
sed 's/^\(>[^ ]*\) .*/\1/g' Dairy_phages_database/dairy_phages_db.fasta > BLAT_DAIRY_PHAGES_DB/dairy_phages_db_clean.fasta

# Step 2: pariwise alignment of contigs by blat
conda activate blat-36
blat BLAT_DAIRY_PHAGES_DB/dairy_phages_db_clean.fasta BLAT_DAIRY_PHAGES_DB/viral_contigs_clean.fasta BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat -out=blast8
conda deactivate

# Step 3: create filelength file (one-liner using BioPython, replace "*.fasta" by your fasta input file, and "*.tab" by your output)
conda activate biopython-1.79
python -c "from Bio import SeqIO;
with open('BLAT_DAIRY_PHAGES_DB/viral_contigs_clean.fasta', 'r') as handle:
    for record in SeqIO.parse(handle, 'fasta'):
        contig_name = record.id;
        contig_size = len(record.seq);
        with open('BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab', 'a') as output_handle:
            output_handle.write(f'{contig_name}\t{contig_size}\n')"
conda deactivate

# Step 4: select the best hit for each contig 
## generate the list of contigs for which we want the best hit
cut -f1 BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | sort -u > BLAT_DAIRY_PHAGES_DB/contigs.txt
### cut -f1 = only keep query names
### sort -u = only keep unique elements

## perform a loop on all the contigs (results are written in the "contig_genome_pairs_TOTAL.tab" file containing : contig_id ; best_hit_accession_number ; sum_of_the_alignement_length ; contig_length ; score)
for i in $(cat BLAT_DAIRY_PHAGES_DB/contigs.txt)
do
	grep $i BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | cut -f1,2,4 |\
	 ./scripts/hashsums | sort -k3nr | head -1
done | \
./scripts/joincol BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab | \
awk 'BEGIN{FS="\t"; OFS="\t"}{score=($3/$4); print $0, score}' > BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_TOTAL.tab
### grep $i = select the line i
### cut -f1,2,4 = only keeps query, subject and the alignment lenght columns
### hashsums = sums all the alignment lengths for the same query/subject pair = gives us the total alignment length
### sort -rn -k 3 = sort numerically (-n) and by decreasing values (-r) according to the 3rd column (-k 3) (sort data by decreasing alignment length)
### head -n 1 = only keep the first line (only keep the best hit)
### joincol = join the current table with the table of [contig name | contig length], the key is the first column (= query name) = the file is now [query name | target name | alignment length | query length]
### awk 'BEGIN{FS="\t"; OFS="\t"}' = before starting, indicates that separatore is tab in the entry (FS) and output (OFS)
### awk '{score=($3/$4); print $0, score}' = create the varialbe score consisting of alignment length / query length, print the line and add the score varialbe 

## variant of the loop that select only contigs having a best hit with a score > a given value
for i in $(cat BLAT_DAIRY_PHAGES_DB/contigs.txt)
do
	grep $i BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | cut -f1,2,4 |\
	 ./scripts/hashsums | sort -k3nr | head -1
done | \
./scripts/joincol BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab | \
awk 'BEGIN{FS="\t"; OFS="\t"}{score=($3/$4); if(score>=0.3) {print $0, score}}' > BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_30.tab

for i in $(cat BLAT_DAIRY_PHAGES_DB/contigs.txt)
do
	grep $i BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | cut -f1,2,4 |\
	 ./scripts/hashsums | sort -k3nr | head -1
done | \
./scripts/joincol BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab | \
awk 'BEGIN{FS="\t"; OFS="\t"}{score=($3/$4); if(score>=0.5) {print $0, score}}' > BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_50.tab

for i in $(cat BLAT_DAIRY_PHAGES_DB/contigs.txt)
do
	grep $i BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | cut -f1,2,4 |\
	 ./scripts/hashsums | sort -k3nr | head -1
done | \
./scripts/joincol BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab | \
awk 'BEGIN{FS="\t"; OFS="\t"}{score=($3/$4); if(score>=0.7) {print $0, score}}' > BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_70.tab

for i in $(cat BLAT_DAIRY_PHAGES_DB/contigs.txt)
do
	grep $i BLAT_DAIRY_PHAGES_DB/contigs_vs_db.blat | cut -f1,2,4 |\
	 ./scripts/hashsums | sort -k3nr | head -1
done | \
./scripts/joincol BLAT_DAIRY_PHAGES_DB/viral_contig_lengths.tab | \
awk 'BEGIN{FS="\t"; OFS="\t"}{score=($3/$4); if(score>=0.9) {print $0, score}}' > BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_90.tab



#!/bin/bash
# Shell to be used for job execution
#$ -S /bin/bash
# job name
#$ -N mapping
# q name
#$ -q short.q
# Export of environmental variables 
#$ -V
# Standard output
#$ -o log/mapping.out
# Error output
#$ -e log/mapping.err
# Run the command from the working directory
#$ -cwd
# Use 32 CPUs
#$ -pe thread 32

ls
mkdir MAPPING_BWA-MEM2

#Step 1 - Create thebwa-0.7.17 index for mapping with bwa-mem2-2.2.1
conda activate bwa-mem2-2.2.1
bwa-mem2 index -p MAPPING_BWA-MEM2/INDEX VIRAL_CONTIGS/viral_contigs.fasta
conda deactivate

#Step 2 - Mapping using bwa-mem2-2.2.1 and count mapped reads on each contig with msamtools-1.0.0
for file in TRIM_READS_BY_SAMPLE/*.fastq
do
	id=$(echo $(basename $file | sed 's/\.fastq//g'))
	entries=$(grep \@ TRIM_READS_BY_SAMPLE/${id}.fastq | wc -l)
	conda activate bwa-mem2-2.2.1
	bwa-mem2 mem MAPPING_BWA-MEM2/INDEX -t 32 -a TRIM_READS_BY_SAMPLE/${id}.fastq > MAPPING_BWA-MEM2/${id}.sam
	conda deactivate
	conda activate msamtools-1.0.0
	msamtools filter -b -u -l 80 -p 95 -z 80 --besthit -S MAPPING_BWA-MEM2/${id}.sam > MAPPING_BWA-MEM2/${id}.bam
	rm -f MAPPING_BWA-MEM2/${id}.sam
	msamtools profile --multi=proportional --label=LABEL --unit=ab --nolen MAPPING_BWA-MEM2/${id}.bam -o MAPPING_BWA-MEM2/results.profile_${id}_proportional.txt.gz --total=$entries
	rm -f MAPPING_BWA-MEM2/${id}.bam
	conda deactivate
done

# Step 3 - gunzip all results.profile output files
gunzip MAPPING_BWA-MEM2/*.gz


#!/bin/bash
# Shell to be used for job execution
#$ -S /bin/bash
# job name
#$ -N identify
# q name
#$ -q short.q
# Export of environmental variables 
#$ -V
# Standard output
#$ -o log/identify.out
# Error output
#$ -e log/identify.err
# Run the command from the working directory
#$ -cwd
# Use 32 CPUs
#$ -pe thread 32

# VIBRANT
conda activate vibrant-1.2.1
VIBRANT_run.py -i derep/contigs_derep.fasta -folder VIBRANT -t 32
conda deactivate
# CheckV
conda activate checkv-0.8.1
checkv end_to_end derep/contigs_derep.fasta CHECKV -d /db/outils/CHECKV-db/checkv-db-v1.1 -t 32
conda deactivate
# Virosorter2
conda activate virsorter2-2.2.4
virsorter setup -d VIRSORTER2/virsorter_db
#virsorter run -w VIRSORTER2 -i derep/contigs_derep.fasta --viral-gene-required --db-dir VIRSORTER2/virsorter_db
virsorter run -w VIRSORTER2 -i derep/contigs_derep.fasta --db-dir VIRSORTER2/virsorter_db --include-groups dsDNAphage,NCLDV,RNA,ssDNA,lavidaviridae --exclude-lt2gene --viral-gene-required --hallmark-required --min-score 0.9 --high-confidence-only -j 32
conda deactivate
# Extract viral contigs
mkdir VIRAL_CONTIGS
cat VIBRANT/VIBRANT_contigs_derep/VIBRANT_results_contigs_derep/VIBRANT_genome_quality_contigs_derep.tsv | grep 'complete\|high\|medium' | awk '{ print $1}' > VIRAL_CONTIGS/List_contigs_Vibrant.txt
cat CHECKV/quality_summary.tsv | awk '{ print $1, $8 }' | grep 'Complete\|High\|Medium' | awk '{ print $1}' > VIRAL_CONTIGS/List_contigs_CheckV.txt
cat VIRSORTER2/final-viral-score.tsv | grep 'full' | awk '{ print $1 }' | cut -d '|' -f1 > VIRAL_CONTIGS/List_contigs_VirSorter.txt
cat VIRAL_CONTIGS/List_contigs_* | sort | uniq > VIRAL_CONTIGS/List_contigs_union.txt
./scripts/fasta_reader_select_P3.py derep/contigs_derep.fasta VIRAL_CONTIGS/List_contigs_union.txt VIRAL_CONTIGS/viral_contigs.fasta

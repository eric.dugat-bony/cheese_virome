#!/bin/bash
# Shell Ã  utiliser pour l'exÃ©cution du job
#$ -S /bin/bash
# Nom du job
#$ -N dereplication
# Nom de la queue
#$ -q short.q
# Export de toutes les variables d'environnement
#$ -V
# Sortie standard
#$ -o log/dereplication.out
# Sortie dâ€™erreur
#$ -e log/dereplication.err
# Lance la commande depuis le rÃ©pertoire oÃ¹ est lancÃ© le script
#$ -cwd
# Utiliser 16 CPUs
#$ -pe thread 16

# Step 1: for contig headers, only keep what's before the first space (spaces are used as separators for the rest of the script)
sed 's/^\(>[^ ]*\) .*/\1/g' Spades_output_combined/all_2Kb.fasta > Spades_output_combined/all_2Kb_clean.fasta

# Step 2: pariwise alignment of contigs by blat
mkdir derep
conda activate blat-36
blat Spades_output_combined/all_2Kb_clean.fasta Spades_output_combined/all_2Kb_clean.fasta derep/contigs.all.blat -out=blast8
conda deactivate

# Step 3: create filelength file (one-liner using BioPython, replace "all_2Kb_clean.fasta" by your fasta input file, and "cheese_lengths.tab" by your output)
conda activate biopython-1.79
python -c "from Bio import SeqIO;
with open('Spades_output_combined/all_2Kb_clean.fasta', 'r') as handle:
    for record in SeqIO.parse(handle, 'fasta'):
        contig_name = record.id;
        contig_size = len(record.seq);
        with open('derep/contig_lengths.tab', 'a') as output_handle:
            output_handle.write(f'{contig_name}\t{contig_size}\n')"
conda deactivate

# Step 4: create the list of chimeras
cut -f1,2,4 derep/contigs.all.blat | ./scripts/hashsums | awk '$1 == $2' | ./scripts/joincol derep/contig_lengths.tab | awk '{if ($3/$4 >= 1.1) print $1}' > derep/List_chimeras.tab

# cut -f1,2,4 = only keeps query, subject and the alignment lenght columns
# hashsums = sums all the alignment lengths for the same query/subject pair = gives us the length for the full contig
# awk '$1 == $2' = only keeps the cases which query and subject are the same contig = only looking at potential chimeras
# joincol = join the current table with the table of [contig name | contig length], the key is the first column (= query name) = the file is now [query name | target name | alignment length | query length]
# awk '{if ($3/$4 >= 1.1) print $1}' = only print first column (= query name) if the alignment length / query length > 1.1 (> 10% of genome repeated), so this ouput the chimeric contig names

# Step 5: generate a new lengths file without chimeras (necessary for clustering step, to avoid keeping the chimeras)
grep -v -w -f derep/List_chimeras.tab derep/contig_lengths.tab > derep/contig_lengths_no_chimeras.tab
# -v = invert the search, only keep the lines that do not have this pattern
# -w = only search for exact pattern
# -f = search for all the strings in the file (next argument)

# Step 6: remove the chimeras & clustering at ~90% coverage * identity
#grep -v -w -f derep/List_chimeras.tab derep/contigs.all.blat | cut -f1,2,4 | ./scripts/hashsums | ./scripts/joincol Spades_output_combined/contig_lengths.tab 2 | sort -k4,4nr -k1,1 | awk '{if ($3/$NF >= .90) print $1, $2}' > derep/contig_pairs.tab
grep -v -w -f derep/List_chimeras.tab derep/contigs.all.blat | cut -f1,2,4 | ./scripts/hashsums | ./scripts/joincol derep/contig_lengths.tab | sort -k4,4nr -k1,1 | awk '{if ($3/$NF >= .90) print $1, $2}' > derep/contig_pairs.tab

# grep -v = invert the search, only keep the lines that do not have this pattern
# grep -w = only search for exact pattern
# grep -f = search for all the strings in the file (next argument)
# the result is a blat table without any chimeras
# cut -f1,2,4 = only keeps query, subject and the alignment lenght columns
# hashsums = sums all the alignment lengths for the same query/subject pair = gives us the length for the full contig
# joincol = join the current table with the table of [contig name | contig length], the key is the second column (= subject name) = the file is now [query name | target name | alignment length | subject length]
# sort -k4,4nr -k1,1 = sort the data in reverse (-r) numerical (-n) order according to the key (-k) which is defined as the 4th column (-k4,4 because we start and end at the 4th field). If there are any equal values, the data are then sorted according to the 1st column (-k1,1).
# awk '{if ($3/$NF >= .90) print $1, $2}' = only if the alignment length / last column (subject length) > 0.9, print the 1st (query name) and 2nd column (subject name). So the output is a list of pairs of contigs that cluster together
# Warning: this command will generate an error message "division by zero attempted". This is NORMAL. This comes from hashsums which generates a 0 at first line. At the sort step, this "0" line is placed at the end of the file, so it doesn't create a problem

# Step 7: go from the pair of similar contigs to a CD-HIT-like cluster file (".clusters" file), and a file with the name of the contigs to keep (".txt" file), python code orignally made by Marie-AgnÃ¨s
./scripts/aggregate.py derep/contig_pairs.tab derep/contig_lengths_no_chimeras.tab derep/clust_out

# Step 8: go from the the CD-HT-like cluster file, python code orignally made by Marie-AgnÃ¨s
./scripts/fasta_reader_select_P3.py Spades_output_combined/all_2Kb_clean.fasta derep/clust_out.txt derep/contigs_derep.fasta

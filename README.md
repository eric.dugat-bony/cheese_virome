# Dynamics of the viral community on the cheese surface during maturation and persistence across production years
This workflow details the bioinformatics and statistical analysis presented in this research article: [link] 

## Input files and directory
- [ ] Directory containing all scripts used in this workflow: 'scripts/'
- [ ] Directory containing the raw sequence data in fastq format: `RAW_DATA/virome` and `RAW_DATA/metabarcoding`

Raw sequence data were deposited at the [sequence read archive (SRA) of the NCBI](https://www.ncbi.nlm.nih.gov/sra/) as part of bioprojects [PRJNA984302](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA984302) (dynamic study) and [PRJNA984735](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA984735) (persistence study)

## Virome data
### Read QC
For each sample, raw reads were first checked for quality using [FastQC](https://github.com/s-andrews/FastQC) version 0.11.9, as follow:
```
conda activate fastqc-0.11.9
fastqc /RAW_DATA/virome/Sample1_R1.fastq.gz -o fastqc_raw/
fastqc /RAW_DATA/virome/Sample1_R2.fastq.gz -o fastqc_raw/
conda deactivate
```

### Cleaning
For each sample, raw paired-end reads were quality filtered using [Trimmomatic](https://github.com/usadellab/Trimmomatic) version 0.39 and stored into the `Trim` directory, as follow: 
```
conda activate trimmomatic-0.39
trimmomatic PE -phred33 /RAW_DATA/virome/Sample1_R1.fastq.gz /RAW_DATA/virome/Sample1_R2.fastq.gz Trim/Sample1_1P.fastq.gz Trim/Sample1_1U.fastq.gz Trim/Sample1_2P.fastq.gz Trim/Sample1_2U.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:20 MINLEN:125
conda deactivate
```
The `TruSeq3-PE.fa` file contained the fasta sequences of the Illumina adaptators used for library preparation: 
```
>PrefixPE/1
TACACTCTTTCCCTACACGACGCTCTTCCGATCT
>PrefixPE/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
```
For each triplicate samples (ripening stage in the dynamics study, production year in the persistence study), clean reads were merged using the `cat` function while keeping the distinction between paired reads from R1 (..._1P.fastq.gz), paired reads from R2 (..._2P.fastq.gz) and unpaired reads (..._1U.fastq.gz and ..._2U.fastq.gz),  as follow:
```
cat Trim/Replicate1_1P.fastq.gz Trim/Replicate2_1P.fastq.gz Trim/Replicate3_1P.fastq.gz > Trim/Triplicate1_1P.fastq.gz
cat Trim/Replicate1_2P.fastq.gz Trim/Replicate2_2P.fastq.gz Trim/Replicate3_2P.fastq.gz > Trim/Triplicate1_2P.fastq.gz
cat Trim/Replicate1_1U.fastq.gz Trim/Replicate2_1U.fastq.gz Trim/Replicate3_1U.fastq.gz Trim/Replicate1_2U.fastq.gz Trim/Replicate2_2U.fastq.gz Trim/Replicate3_2U.fastq.gz > Trim/Triplicate1_U.fastq.gz
```

### Sub-sampling
Sub-sampling can improve assembly of some contigs, in particular those that are very abundant in the dataset. Sub-sampling was performed using [seqtk](https://github.com/lh3/seqtk) version 1.3 to 1.5 million, 150,000 or 15,000 trimmed reads per triplicate sample, and results were stored in `Trim/sub/` directory. Here is an example for subsampling 15,000 reads (5,000 1P + 5,000 2P + 5,000 U) for Triplicate 1: 
```
conda activate seqtk-1.3
seqtk sample -s100 Trim/Triplicate1_1P.fastq 5000 > Trim/sub/Triplicate1_1P_sub_5K.fastq
seqtk sample -s100 Trim/Triplicate1_2P.fastq 5000 > Trim/sub/Triplicate1_2P_sub_5K.fastq
seqtk sample -s100 Trim/Triplicate1_U.fastq 5000 > Trim/sub/Triplicate1_U_sub_5K.fastq
conda deactivate
```

### Assembly
A single assembly was computed for each triplicate with [Spades](https://github.com/ablab/spades) version 3.15.3 by using either the complete dataset of trimmed reads available or after subsampling. Results were stored in `Assembly/` directory. Here is an example for the complete dataset of Triplicate 1: 
```
conda activate spades-3.15.3
spades.py --only-assembler --tmp-dir /tmp -1 Trim/Triplicate1_1P.fastq -2 Trim/Triplicate1_2P.fastq.gz -s Trim/Triplicate1_U.fastq.gz -o Assembly/Triplicate1 -k 21,33,55,77,99,127
conda deactivate 
```
For each assembly, contigs >2Kb were selected and a prefix was added to each contig name to save the information of the original assembly by using the script `fasta_reader_rename_P3.py`
```
fasta_reader_rename_P3.py assembly_file.fasta prefix 
```
Then all output fasta files were concatenated using the `cat` function and saved in `Spades_output_combined/all_2Kb.fasta`

### Clustering of contigs into species-level vOTUs 
This step is based on the code previously described by [Shiraz Shah](https://github.com/shiraz-shah/VFCs) which was embedded in the bash script `script_dereplication_contigs_by_blat.sh` Briefly, we used [BLAT](https://github.com/djhshih/blat) version 36 to do an all-against-all alignment and the output from BLAT was used to build ~95% sequence clusters (90% coverage * identity), while avoiding the selection of chimeric assemblies as OTU representatives.

The script can be run using the following command after modifying the paths for input and output files:
```
./scripts/script_dereplication_contigs_by_blat.sh
```
For each cluster, the longest sequence was selected as representative sequence and saved in `derep/contigs_derep.fasta`

### Selection of viral contigs 
This step was used to select potential viral contigs using a combination of [VIBRANT](https://github.com/AnantharamanLab/VIBRANT) version 1.2.1, [VirSorter2](https://github.com/jiarong/VirSorter2) version 2.2.4 and [CheckV](https://bitbucket.org/berkeleylab/checkv/src/master/) version 0.8.1. Contigs were selected if they meet at least one of the following criteria: declared “complete”, “high” or “medium” quality by either VIBRANT or CheckV, declared “full” by VirSorter2. All command lines were embedded in the bash script `script_identification_of_viral_contigs.sh`

The script can be run using the following command after modifying the paths for input and output files:
```
./scripts/script_identification_of_viral_contigs.sh
```
Viral contigs were saved in `VIRAL_CONTIGS/viral_contigs.fasta`

### Host prediction
All viral contigs were analyzed for hot prediction using [iPHoP](https://bitbucket.org/srouxjgi/iphop/src/main/) version 1.2.0. results were stored in `IPHOP/`
The command lines were embedded in the bash script `script_host_prediction.sh` that can be run as follow:
```
./scripts/script_host_prediction.sh
```
Results were stored in `IPHOP/`

### Sequence comparison against dairy phages 
We used [BLAT](https://github.com/djhshih/blat) version 36 to align viral contgis against a in-house database containing genomic sequences from 32 phages isolated from dairy products (database is stored in `Dairy_phages_database/`). We then retained for each entry the best hit sequence if Blat score (coverage * identity) was superior to 30%. The command lines were embedded in the bash script `script_blat_dairy_phages_db.sh` that can be run as follow:
```
./scripts/script_blat_dairy_phages_db.sh
```
Results were stored a tabular format in `BLAT_DAIRY_PHAGES_DB/contig_genome_pairs_30.tab`

### Mapping of reads (trimmed) against the viral contigs set
Mapping of trimmed reads against viral contigs was performed using [bwa-mem2](https://github.com/bwa-mem2/bwa-mem2) version 2.2.1 and raw counts were extracted using [msamtools](https://github.com/arumugamlab/msamtools) version 1.0.0. All command lines for creating the index, mapping and extracting raw counts were embedded in the bash script `mapping.sh`
 
The script can be run using the following command after modifying the paths for input and output files:
```
./scripts/mapping.sh
```

### Create a Phyloseq object and perform data normalization

The R script `normalization_whole_dataset.rmd`, saved in `scripts/` directory, was used to construct [plyloseq](https://github.com/joey711/phyloseq) objects (phyloseq version 1.44.0) from counts data obtained form cheese viromes (all samples from dynamic and persistence studies). Metadata associated to vOTUs included iphop predictions, vibrant annotations, blat best hit against the dairy phages database and manual annotations. One phyloseq object containing raw counts and one containing transformed data (TPM: data rarefied, normalized by contig length and transformed to proportions) were produced and saved as R object in rds format in `PHYLOSEQ_ANALYSIS/` directory. Some basic analyses of the whole dataset were also included in this script. 

The script require installation of the following R packages: [tidyverse](https://github.com/tidyverse/tidyverse), [plyloseq](https://github.com/joey711/phyloseq), [DT](https://github.com/rstudio/DT) and [ggVennDiagram](https://github.com/gaospecial/ggVennDiagram)

## 16S metabarcoding data

Metabarcoding data from dynamics and persistence studies were processed separately using [FROGS](https://github.com/geraldinepascal/FROGS) version 4.1.0. For each study, a `tar` archive containing all raw FASTQ files was first created and stored in `FROGS/` directory, and the following command lines were used to process the data:

```
#Step 1: Reads preprocessing
preprocess.py illumina --input-archive FROGS/data.tar.gz --min-amplicon-size 400 --max-amplicon-size 517 --merge-software vsearch --mismatch-rate 0.1 --five-prim-primer ACGGRAGGCWGCAG --three-prim-primer AGGATTAGATACCCTGGTA --R1-size 250 --R2-size 250 --nb-cpus 16 --output-dereplicated FROGS/preprocess.fasta --output-count FROGS/preprocess.tsv --summary FROGS/preprocess.html --log-file FROGS/preprocess.log

#Step 2: Reads clustering
clustering.py --input-fasta FROGS/preprocess.fasta --input-count FROGS/preprocess.tsv --distance 1 --fastidious --nb-cpus 16 --log-file FROGS/clustering.log --output-biom FROGS/clustering.biom --output-fasta FROGS/clustering.fasta --output-compo FROGS/clustering_compositions.tsv

#Step 3: Chimera removal
remove_chimera.py --input-fasta FROGS/clustering.fasta --input-biom FROGS/clustering.biom --non-chimera FROGS/remove_chimera.fasta --nb-cpus 8 --log-file FROGS/remove_chimera.log --out-abundance FROGS/remove_chimera.biom --summary FROGS/remove_chimera.html

#Step 4: Abundance-based filter
cluster_filters.py --input-fasta FROGS/remove_chimera.fasta --input-biom FROGS/remove_chimera.biom --output-fasta FROGS/filters.fasta --nb-cpus 4 --log-file FROGS/filters.log --output-biom FROGS/filters.biom --summary FROGS/filters.html --excluded FROGS/filters_excluded.tsv --min-abundance 5e-05

#Step 5: Affiliation againts silva database version 138.1
taxonomic_affiliation.py --input-fasta FROGS/filters.fasta --input-biom FROGS/filters.biom --nb-cpus 16 --java-mem 2 --log-file FROGS/affiliation.log --output-biom FROGS/affiliation.biom --summary FROGS/affiliation.html --reference /db/outils/FROGS/assignation/silva_138.1_16S_pintail100/silva_138.1_16S_pintail100.fasta --taxonomy-ranks Domain Phylum Class Order Family Genus Species  

#Transform abundance table to phyloseq object
phyloseq_import_data.py --biomfile FROGS/affiliation.biom --samplefile FROGS/sample_data.tsv --ranks Kingdom Phylum Class Order Family Genus Species --html FROGS/pyloseq.html --rdata FROGS/phyloseq.rdata
```

The resulting phyloseq objects were saved in `16S_datasets/Dynamic_study/16S_Dynamic_dataset.rdata` and `16S_datasets/Persistence_study/16S_Persistence_dataset.rdata`

## Dynamics study of viral and bacterial communities during ripening

The R script `normalization_whole_dataset.rmd`, saved in `scripts/` directory, was used to perform statistical analysis of the dynamics study described in the article and to generate the corresponding figures (saved in `PHYLOSEQ_ANALYSIS/` directory). This analysis included the following steps:
- [ ] Comparison of the composition of viral communities from samples collected at different steps of the ripening process (Beta-diversity)
- [ ] Visualization of the viral community structure using a heatmap
- [ ] Comparison of the diversity of viral communities from samples collected at different steps of the ripening process (Alpha-diversity)
- [ ] Differential analysis of the virome data
- [ ] Visualization of the bacterial community structure using a composition barplot
- [ ] Visualization of the microbial counts using boxplots

The script require installation of the following R packages: [plyloseq](https://github.com/joey711/phyloseq), [plyloseq.extended](https://github.com/mahendra-mariadassou/phyloseq-extended), [tidyverse](https://github.com/tidyverse/tidyverse), [vegan](https://github.com/vegandevs/vegan), [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html), [DT](https://github.com/rstudio/DT), [gridExtra](https://cran.r-project.org/package=gridExtra), [cowplot](https://wilkelab.org/cowplot/), [rstatix](https://rpkgs.datanovia.com/rstatix/), [pheatmap](https://CRAN.R-project.org/package=pheatmap), [ggplotify](https://github.com/GuangchuangYu/ggplotify), [ggpubr](https://github.com/kassambara/ggpubr)

## Persistence of the virome accross production years

The R script `cheese_virome_persistence_study.rmd`, saved in `scripts/` directory, was used to perform statistical analysis of the persistence study described in the article and to generate the corresponding figure (saved in `PHYLOSEQ_ANALYSIS/` directory). This analysis included the following steps:
- [ ] Comparison of the composition of viral communities from samples collected at different production years (Beta-diversity)
- [ ] Visualization of the viral community structure using a heatmap
- [ ] Identification of a core virome (Venn diagram)
- [ ] Identification of specific vOTUs to a particular production year
- [ ] Visualization of the bacterial community structure using a composition barplot

The script require installation of the following R packages: [plyloseq](https://github.com/joey711/phyloseq), [plyloseq.extended](https://github.com/mahendra-mariadassou/phyloseq-extended), [tidyverse](https://github.com/tidyverse/tidyverse), [vegan](https://github.com/vegandevs/vegan), [S4Vectors](https://bioconductor.org/packages/S4Vectors), [DT](https://github.com/rstudio/DT), [cowplot](https://wilkelab.org/cowplot/), [ggVennDiagram](https://github.com/gaospecial/ggVennDiagram), [pheatmap](https://CRAN.R-project.org/package=pheatmap), [ggplotify](https://github.com/GuangchuangYu/ggplotify)
